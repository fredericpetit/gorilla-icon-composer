# Gorilla icon - an automated repository for Composer availability.

![gorilla](assets/img/gorilla.png)

_Icons available in SVG & PNG formats._

| param | value |
| ------ | ------ |
| **URL home original author** | [https://jimmac.eu/](https://jimmac.eu/) (Jakub Steiner) |
| **URL git fredericpetit** | [https://gitlab.com/fredericpetit/gorilla-icon-composer/](https://gitlab.com/fredericpetit/gorilla-icon-composer/) |
| **NAMESPACE** (composer) | [gorilla-icon-composer](https://packagist.org/packages/fredericpetit/gorilla-icon-composer/) |
| **DATE** ('build' commit) | <span id="date">sam. 30 mars 2024 02:42:59 CET</span> |
| **SCHEDULE** (pipeline) | x |

## Usage.
- Add namespace to composer configuration and perform setup.
- Icons are in _./assets/img/gorilla/_.
- Old GNOME desktop environment configuration is in _./gnome/_.